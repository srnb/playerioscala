package tf.bug.android.replacements;

public class LogReplacement {

    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;
    public static final int ASSERT = 7;

    public static int v(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.v(tag, msg);
    }

    public static int v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.v(tag, msg, tr);
    }

    public static int d(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.d(tag, msg);
    }

    public static int d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.d(tag, msg, tr);
    }

    public static int i(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.i(tag, msg);
    }

    public static int i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.i(tag, msg, tr);
    }

    public static int w(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.w(tag, msg);
    }

    public static int w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.w(tag, msg, tr);
    }

    public static int w(java.lang.String tag, java.lang.Throwable tr) {
        return LogReplacementHelper.w(tag, tr);
    }

    public static int e(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.e(tag, msg);
    }

    public static int e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.e(tag, msg, tr);
    }

    public static int wtf(java.lang.String tag, java.lang.String msg) {
        return LogReplacementHelper.wtf(tag, msg);
    }

    public static int wtf(java.lang.String tag, java.lang.Throwable tr) {
        return LogReplacementHelper.wtf(tag, tr);
    }

    public static int wtf(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        return LogReplacementHelper.wtf(tag, msg, tr);
    }

    public static java.lang.String getStackTraceString(java.lang.Throwable tr) {
        return LogReplacementHelper.getStackTraceString(tr);
    }

}
