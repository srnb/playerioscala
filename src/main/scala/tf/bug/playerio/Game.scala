package tf.bug.playerio

import java.lang.reflect.Method

import android.util.Log
import cats.effect.IO
import com.playerio
import com.playerio.{Callback, PlayerIO, PlayerIOError}
import net.bytebuddy.ByteBuddy
import net.bytebuddy.agent.ByteBuddyAgent
import net.bytebuddy.description.method.MethodDescription
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy
import net.bytebuddy.implementation.MethodDelegation
import net.bytebuddy.matcher.ElementMatchers
import tf.bug.android.replacements.LogReplacement

import scala.collection.JavaConverters._

object Game {

  def load(): Unit = {
    ByteBuddyAgent.install()
    def methodShortName(m: Method) = m.getName + "[" + m.getTypeParameters.map(_.getName).mkString(",") + "]"
    val origMethods = classOf[Log].getDeclaredMethods
    val replacementMethods = classOf[LogReplacement].getDeclaredMethods
    val bb = new ByteBuddy().redefine(classOf[Log])
    val mm = origMethods.filter(om => replacementMethods.map(methodShortName).contains(methodShortName(om)))
    val newbb = mm.foldLeft(bb) {
      case (bacc, om) =>
        bacc.method(ElementMatchers.is[MethodDescriptor](om)).intercept(MethodDelegation.to(classOf[LogReplacement]))
    }
    newbb.make().load(classOf[Log].getClassLoader, ClassReloadingStrategy.fromInstalledAgent())
  }

}

case class Game(id: String) {

  def authenticate(
    authenticationArguments: Map[String, String],
    playerInsightSegments: Array[String] = Array()): IO[Client] = {
    IO.async[Client] { callback =>
      PlayerIO.authenticate(
        null,
        id,
        "public",
        authenticationArguments.asJava,
        playerInsightSegments,
        new Callback[playerio.Client] {
          override def onSuccess(t: playerio.Client): Unit = callback(Right(new Client(t)))
          override def onError(e: PlayerIOError): Unit = callback(Left(e))
        }
      )
    }
  }

}
