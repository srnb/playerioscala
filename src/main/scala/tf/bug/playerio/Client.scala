package tf.bug.playerio

import com.playerio

class Client(private val internal: playerio.Client) {

  lazy val multiplayer: Multiplayer = new Multiplayer(internal.multiplayer)

}
