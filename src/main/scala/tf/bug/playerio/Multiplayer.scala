package tf.bug.playerio

import cats.effect.IO
import com.playerio
import com.playerio.{Callback, PlayerIOError, ServerEndpoint}

import scala.collection.JavaConverters._

class Multiplayer(private val internal: playerio.Multiplayer) {

  def rooms: MultiplayerRooms = new MultiplayerRooms(internal)

  def join(roomId: String, joinData: Map[String, String]): IO[Connection] = {
    IO.async[Connection] { callback =>
      internal.joinRoom(
        roomId,
        joinData.asJava,
        new Callback[playerio.Connection] {
          override def onSuccess(t: playerio.Connection): Unit = callback(Right(new Connection(t)))
          override def onError(e: PlayerIOError): Unit = callback(Left(e))
        }
      )
    }
  }

  def developmentServer_=(server: ServerEndpoint): Unit = internal.setDevelopmentServer(server)

}

class MultiplayerRooms(private val internal: playerio.Multiplayer) {

  def apply(
    roomType: String,
    resultLimit: Int,
    resultOffset: Int = 0,
    searchCriteria: Map[String, String] = Map()): IO[Array[RoomInfo]] = {
    IO.async[Array[RoomInfo]] { callback =>
      internal.listRooms(
        roomType,
        searchCriteria.asJava,
        resultLimit,
        resultOffset,
        new Callback[Array[playerio.RoomInfo]] {
          override def onSuccess(t: Array[playerio.RoomInfo]): Unit = callback(Right(t.map(new RoomInfo(_))))
          override def onError(e: PlayerIOError): Unit = callback(Left(e))
        }
      )
    }
  }

  def +=(
    roomId: String,
    roomType: String,
    visible: Boolean = true,
    roomData: Map[String, String] = Map()): IO[String] = {
    IO.async[String] { callback =>
      internal.createRoom(
        roomId,
        roomType,
        visible,
        roomData.asJava,
        new Callback[String] {
          override def onSuccess(t: String): Unit = callback(Right(t))
          override def onError(e: PlayerIOError): Unit = callback(Left(e))
        }
      )
    }
  }

}
