package tf.bug.playerio

import com.playerio

import scala.collection.JavaConverters._

case class RoomInfo(id: String, roomType: String, onlineUsers: Int, roomData: Map[String, String]) {

  def this(internal: playerio.RoomInfo) {
    this(internal.getId, internal.getRoomType, internal.getOnlineUsers, internal.getRoomData.asScala.toMap)
  }

}
