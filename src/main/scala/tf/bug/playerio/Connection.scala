package tf.bug.playerio

import com.playerio
import com.playerio.Message

class Connection(private val internal: playerio.Connection) {

  def messageListeners: MessageListeners = new MessageListeners(internal)

  def disconnect(): Unit = internal.disconnect()

  def ++(message: playerio.Message): Unit = internal.send(message)
  def ++(messageType: String, args: AnyRef*): Unit = internal.send(messageType, args: _*)

}

class MessageListeners(private val internal: playerio.Connection) {

  def +=(messageType: String, handler: playerio.Message => Unit): Unit = {
    internal.addMessageListener(messageType, (message: Message) => handler(message))
  }

}
