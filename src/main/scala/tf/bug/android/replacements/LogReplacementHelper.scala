package tf.bug.android.replacements

object LogReplacementHelper {
  val VERBOSE = 2
  val DEBUG = 3
  val INFO = 4
  val WARN = 5
  val ERROR = 6
  val ASSERT = 7

  def v(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def v(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def d(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def d(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def i(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def i(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def w(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def w(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def isLoggable(tag: String, level: Int): Boolean = true

  def w(tag: String, tr: Throwable): Int = { println(s"[$tag] $tr"); 0 }

  def e(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def e(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def wtf(tag: String, msg: String): Int = { println(s"[$tag] $msg"); 0 }

  def wtf(tag: String, tr: Throwable): Int = { println(s"[$tag] $tr"); 0 }

  def wtf(tag: String, msg: String, tr: Throwable): Int = { println(s"[$tag] $msg: $tr"); 0 }

  def getStackTraceString(tr: Throwable): String = tr.getStackTrace.map(_.toString).mkString("\n")

}
