lazy val root = (project in file(".")).settings(
  name         := "playerioscala",
  version      := "0.1.0",
  scalaVersion := "2.12.6",
  // https://mvnrepository.com/artifact/com.google.android/android
  libraryDependencies ++= Seq(
    "com.google.android"             % "android" % "4.1.1.4",
    "net.bytebuddy"                  % "byte-buddy" % "1.8.22",
    "net.bytebuddy"                  % "byte-buddy-agent" % "1.8.22",
    "org.typelevel" %% "cats-core"   % "1.3.1",
    "org.typelevel" %% "cats-effect" % "1.0.0"
  ),
  scalacOptions += "-Ypartial-unification",
)
